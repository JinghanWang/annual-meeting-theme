<?php
/**
 * @package Skeleton WordPress Theme Framework
 * @subpackage skeleton
 * @author Simple Themes - www.simplethemes.com
 *
 * Layout Hooks:
 *
 * skeleton_above_header // Opening header wrapper
 * skeleton_header // header tag and logo/header text
 * skeleton_header_extras // Additional content may be added to the header
 * skeleton_below_header // Closing header wrapper
 * skeleton_navbar // main menu wrapper
 * skeleton_before_content // Opening content wrapper
 * skeleton_after_content // Closing content wrapper
 * skeleton_before_sidebar // Opening sidebar wrapper
 * skeleton_after_sidebar // Closing sidebar wrapper
 * skeleton_footer // Footer
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, skeleton_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage skeleton
 * @since skeleton 2.0
 */

// function my_custom_creditlink(){
// 	$credits = 'Site by <a href="http://example.org">Your Company Name</a>';
// 	return $credits;
// }
// add_filter('skeleton_author_credits','my_custom_creditlink');


// For completely overriding the theme options and removal of inline styles
// You must dequeue custom.css and redeclare these styles in your child theme
//

add_action( 'wp_enqueue_scripts', 'remove_theme_options', 11);
function remove_theme_options() {
    wp_dequeue_style( 'skeleton-custom' );
}

/**
 * Embed filter js
 */
function enqueue_scripts() {
    // wp_register_script( 'programs_filter', get_stylesheet_directory_uri() . '/programs_filter.js', array( 'jquery' ), '1.0', true );
    wp_register_script( 'programs_filter_all', get_stylesheet_directory_uri() . '/programs_filter_all.js', array( 'jquery' ), '1.0', true );
    if ( is_page(37) || is_page(478) ) { 
        wp_enqueue_script( 'programs_filter_all' );
    // } else if (is_page(478)) {
    // 	wp_enqueue_script( 'programs_filter_all' );
    }
}
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );

/**
 * Override the "Continue Reading" link to "More" for excerpts
 */
function skeleton_continue_reading_link() {
	return ' <a href="'. get_permalink() . '" class="feature-panel-button">' . __( 'More >', 'smpl' ) . '</a>';
}

/**
 * Simple Notification Plugin Action Override
 */
remove_action('wp_footer', 'pippin_display_notice');
add_action('notification_bar', 'pippin_display_notice');

/**
 *  Unhook Default Footer
 */
function unhook_skeleton_footer() {
	remove_action('wp_footer', 'skeleton_footer', 1);
}
add_action('after_setup_theme', 'unhook_skeleton_footer');


?>