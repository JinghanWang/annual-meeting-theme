<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the wordpress construct of pages
 * and that other 'pages' on your wordpress site will use a
 * different template.
 *
 * @package Skeleton WordPress Theme
 * @subpackage skeleton
 * @author Simple Themes - www.simplethemes.com
 */
// You can override via functions.php conditionals or define:
// $columns = 'four';

get_header();
?>

<div class="wrapper-960">
<?php
do_action('skeleton_before_content');
get_template_part( 'loop', 'page' );
do_action('skeleton_after_content');
get_sidebar('page');
?>
<div class="clear"></div>
</div>
<div id="twitter-feed">
	<?php echo do_shortcode('[ap-twitter-feed-pro-slider slide_duration="7000"]'); ?>
</div>

<?php
get_footer();
?>