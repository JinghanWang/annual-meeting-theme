<div class="clear"></div>
<div id="footer-logo" class="wrapper-960">
	<?php get_sidebar( 'footer' );?>
</div>	
<div id="footer-wrapper">
	<div class="clear"></div>
		<?php
		$footerwidgets = is_active_sidebar('footer-widget-area-1') + is_active_sidebar('footer-widget-area-2') + is_active_sidebar('footer-widget-area-3') + is_active_sidebar('footer-widget-area-4');
		$class = ($footerwidgets == '0' ? 'noborder' : 'normal');
		echo '<div id="footer" class="'.$class.' sixteen columns">';

		$defaults = array(
			'theme_location' => 'footer',
			'container'      => 'div',
			'container_id'   => 'footermenu',
			'menu_class'     => 'menu',
			'echo'           => true,
			'fallback_cb'    => false,
			'depth'          => 1);
		wp_nav_menu($defaults);
		?>
		<div class="wrapper-960">
			<div id="footer-site-logo">
				<?php
				$skeleton_logo = '<a class="logotype-img" href="http://www.aam-us.org/" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" rel="home">';
				$skeleton_logo .= '<img src="'.skeleton_options( 'logotype' ).'" alt="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'"></a>';
				echo apply_filters ( 'skeleton_child_logo', $skeleton_logo);
				?>
			</div>
			<div id="footer-text">
				<p>
					<a href="http://aam-us.org/copyright-statement">Copyright Statement</a>  | <a href="http://aam-us.org/privacy-policy">Privacy Policy</a> | <a href="http://aam-us.org/terms">Terms of Use</a>
					<span style="margin-left: 2em;">Join the Conversation <a href="https://twitter.com/hashtag/aam2016">#aam2016</a></span>
				</p>
			</div>
		</div>
	</div>
</div>

<?php wp_footer();?>

</body>
</html>
