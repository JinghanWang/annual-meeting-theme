<?php
/**
 * Template Name: Programs Filter Template
 * This is the template that displays programs filter pages.
 *
 * @package Skeleton WordPress Theme
 * @subpackage skeleton
 * @author Simple Themes - www.simplethemes.com
 */

get_header();

?>

<div class="wrapper-960">
	<a id="top"></a>
	<div id="content">
		<?php get_template_part( 'loop', 'page' );?>
	</div>
	<div id="programs-filter">
		<div id="search-filter">
			<div id="search-box">
				<h5 class="search-label">Enter your keywords</h5>
				<input type="text" placeholder="Please enter your keyword">
			</div>
			<div id="search-checkbox">
				<div id="checkbox-type" class="search-checkbox">
					<h5 class="search-label">Filter by Type</h5>
					<div class="search-filter-container">
						<input type="checkbox" name="event" value="506"><label for="event">Event</label><br>
						<input type="checkbox" name="session" value="505"><label for="event">Session</label>
					</div>
				</div>
				<div id="checkbox-track" class="search-checkbox">
					<h5 class="search-label">Filter by Track</h5>
					<div class="search-filter-container">
						<input type="checkbox" name="career management" value="541"><label for="career management">Career Management</label><br>
						<input type="checkbox" name="collections management" value="606"><label for="collections management">Collections Management</label><br>
						<input type="checkbox" name="curatorial-practice" value="591"><label for="curatorial-practice">Curatorial Practice</label><br>
						<input type="checkbox" name="development and membership" value="543"><label for="development and membership">Development and Membership</label><br>
						<input type="checkbox" name="education-audience-research-evaluation" value="587"><label for="education-audience-research-evaluation">Education, Audience Research & Evaluation</label><br>
						<input type="checkbox" name="exhibit-planning-and-design" value="545"><label for="exhibit-planning-and-design">Exhibit Planning and Design</label><br>
						<input type="checkbox" name="facilities-management" value="592"><label for="facilities-management">Facilities Management</label><br>
						<input type="checkbox" name="management-and-administration" value="589"><label for="management-and-administration">Management and Administration</label><br>
						<input type="checkbox" name="forces-of-change" value="548"><label for="forces-of-change">Forces of Change</label><br>
						<input type="checkbox" name="museum-director" value="602"><label for="museum-director">Museum Director</label><br>
						<input type="checkbox" name="marketing-and-community-engagement" value="590"><label for="marketing-and-community-engagement">Marketing and Community Engagement</label><br>
						<input type="checkbox" name="media-and-technology" value="551"><label for="media-and-technology">Media and Technology</label>
					</div>
				</div>
				<div id="checkbox-day" class="search-checkbox">
					<h5 class="search-label">Filter by Day</h5>
					<div class="search-filter-container">
						<input type="checkbox" name="wednesday" value="Wednesday"><label for="wednesday">Wednesday</label><br>
						<input type="checkbox" name="thursday" value="Thursday"><label for="thursday">Thursday</label><br>
						<input type="checkbox" name="friday" value="Friday"><label for="friday">Friday</label><br>
						<input type="checkbox" name="saturday" value="Saturday"><label for="saturday">Saturday</label><br>
						<input type="checkbox" name="sunday" value="Sunday"><label for="sunday">Sunday</label>
					</div>
				</div>		
			</div>
			<div id="search-button">
				<input type="submit" value="Search">
			</div>
		</div>
		<div id="search-result-container" class="search-result">
			<h2>Search Results</h2>
			<div id="search-result" class="search-result">
				<p>
					Select filters or enter keywords and click "Search" to begin.
				</p>
			</div>
		</div>
	</div>
</div>

<?php
get_footer();
?>