function apiData(result) {
	listResult(result);
};
function listResult (result) {
		
	var output = result["AnnualMeetings"];
	var container, expand_container, more, title, track, speaker, room, price, startDay, startDate, track1, track2, venue, description, sTime;	
	jQuery('#search-result').empty();
	console.log(output);

	if (typeof output === "undefined") {
		jQuery('#search-result').html('No results found. Try removing filters and searching again.');
	} else {
		jQuery.each(output, function(index, value) {
			var output_single = value;

			container = jQuery('<div class="result-items"></div>');
			expand_container = jQuery('<div class="result-expand"></div>');
			more = jQuery('<div class="result-more">Details</div>');
			more.css("cursor", "pointer");

			title = jQuery('<h4></h4>').html(output_single["MeetingTitle"]);
			track = jQuery('<div class="result-track result-details"></div>').html("Track(s): " + output_single["Track"]);
			startDay = jQuery('<div class="result-startDay result-details"></div>').html(output_single["StartDay"]);
			sTime = jQuery('<div class="result-sTime result-details"></div>').html(output_single["sTime"]);
			room = jQuery('<div class="result-room result-details"></div>').html(output_single["MeetingRoom"]);
			venue = jQuery('<div class="result-venue result-details"></div>').html(output_single["Venue"]);
			track1 = jQuery('<div class="result-track1 result-details"></div>').html(output_single["Track1"]);
			track2 = jQuery('<div class="result-track2 result-details"></div>').html(output_single["Track2"]);
			price = jQuery('<div class="result-price result-details"></div>').html(output_single["Price"]);
			speaker = jQuery('<div class="result-speaker result-details"></div>').html(output_single["MeetingDSpeakers"]);
			// startDate = jQuery('<div class="result-startDate result-details"></div>').html(output_single["Startdate"]);
			description = jQuery('<div class="result-description result-details"></div>').html(output_single["VerboseDescription"]);
			
			jQuery('#search-result').append(container);
			container.append(title, track, startDay, sTime, room, venue, track1, track2, more, expand_container);
			expand_container.append(speaker, description, price).hide();
		});
		jQuery('.result-more').click(function() {
			jQuery(this).next().slideToggle();
		});
	}
}
function urlQuery (data) {
	
	var qd = {};
	location.search.substr(1).split("&").forEach(function(item) {
		(item.split("=")[0] in qd) ? qd[item.split("=")[0]].push(item.split("=")[1]) : qd[item.split("=")[0]] = [item.split("=")[1]]
	});
	data["TopicCodeIDs"] = qd["TopicCodeIDs"];
	data["Days"] = qd["Days"];
	data["TypeIDs"] = qd["TypeIDs"];
	return data;
}

var api_url = "http://api.aam-us.org/AptifyRestService/AnnualMeeting.svc/GetAnnualMeetingDetailsByClass?callback=apiData";

(function($){

	var data = {"TopicCodeIDs": [],
				"Days": [],
				"TypeIDs": [],
				"SearchText": ""	
                };
	function apirequest (data) {
		
		data = JSON.stringify(data);
		$.ajax({
			type: "POST",
			url: api_url,
			crossDomain: true,
			data: { "annualMeeting": data},
			dataType:'jsonp',
			contentType: "application/json",
			success: apiData
		});
	}
	function filterCollapse () {
		$('.search-checkbox .search-label').click(function () {
			$(this).next().slideToggle();
		});
		if ($(window).width() < 769) {
			$('.search-filter-container').hide();
		} else {
			$('#checkbox-track .search-filter-container').hide();
			$('#checkbox-day .search-filter-container').hide();
		}
	}
	function closeFilterSearch () {
		if ($(window).width() < 769) {
			$.each($('.search-filter-container'), function() {
				if ($(this).is(':visible')) {
					$(this).hide();
				}
			});
		}
	}
	$(document).ready(function(){

		filterCollapse();

		var topic = [],
			specialAudience = [],
			day = [],
			typeID = [];

		urlQuery(data);

		if (data["TopicCodeIDs"] != undefined || data["Days"] != undefined || data["TypeIDs"] != undefined) {
			
			$('#search-result').empty();
			$('#search-result').html('Searching programs in database...');

			apirequest(data);

			if (data["TopicCodeIDs"] != undefined) {
				$.each(data["TopicCodeIDs"], function(index, value) {
					$.each($('#checkbox-track input'), function() {
						var checkboxValue = $(this).val();
						if (checkboxValue == value) {
							$(this).prop('checked', true);
						}
					});
					topic.push(value);
				});
			}
			if (data["Days"] != undefined) {
				$.each(data["Days"], function(index, value) {
					$.each($('#checkbox-day input'), function() {
						var checkboxValue = $(this).val();
						if (checkboxValue == value) {
							$(this).prop('checked', true);
						}
					});
					day.push(value);
				});
			}
			if (data["TypeIDs"] != undefined) {
				$.each(data["TypeIDs"], function(index, value) {
					$.each($('#checkbox-type input'), function() {
						var checkboxValue = $(this).val();
						if (checkboxValue == value) {
							$(this).prop('checked', true);
						}
					});
					typeID.push(value);
				});
			}
		}

		apirequest(data);

		$('#checkbox-type input[type="checkbox"]').change(function(){
			var temp_input = $(this).val();
			if ($(this).prop("checked")) {
				if ($.inArray(temp_input, typeID) == -1) {
					typeID.push(temp_input);
				}
			}
			else {
				if ($.inArray(temp_input, typeID) != -1) {
					typeID.splice($.inArray(temp_input, typeID), 1);
				}
			}
		});
		$('#checkbox-track input[type="checkbox"]').change(function(){
			var temp_input = $(this).val();
			if ($(this).prop("checked")) {
				if ($.inArray(temp_input, topic) == -1) {
					topic.push(temp_input);
				}
			}
			else {
				if ($.inArray(temp_input, topic) != -1) {
					topic.splice($.inArray(temp_input, topic), 1);
				}
			}
		});
		$('#checkbox-day input[type="checkbox"]').change(function(){
			var temp_input = $(this).val();
			if ($(this).prop("checked")) {
				if ($.inArray(temp_input, day) == -1) {
					day.push(temp_input);
				}
			}
			else {
				if ($.inArray(temp_input, day) != -1) {
					day.splice($.inArray(temp_input, day), 1);
				}
			}
		});
		$('#search-button input[value="Search"]').click(function(){
			$('#search-result').empty();
			$('#search-result').html('Searching programs in database...');
			closeFilterSearch();

            var searchText = $('#search-box input[type="text"]').val();
			data["TopicCodeIDs"] = topic;
			data["Days"] = day;
			data["TypeIDs"] = typeID;
			data["SearchText"] = searchText;

			apirequest(data);
		});
	});
})(jQuery);

