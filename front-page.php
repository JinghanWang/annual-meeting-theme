<?php
/**
 * Template Name: Front Page Template
 * This is the template that displays all pages by default.
 * Please note that this is the wordpress construct of pages
 * and that other 'pages' on your wordpress site will use a
 * different template.
 *
 * @package Skeleton WordPress Theme
 * @subpackage skeleton
 * @author Simple Themes - www.simplethemes.com
 */
// You can override via functions.php conditionals or define:
// $columns = 'four';

get_header();
?>

<div id="notification-wrapper">
	<?php do_action('notification_bar');?>
</div>

<div id="feature-area">
	<?php echo do_shortcode('[masterslider id="1"]');?>
</div>

<div id="call-to-action" class="call-to-action">
	<div class="single-call-to-action"><?php echo gcb(13); ?></div>
	<div class="single-call-to-action"><?php echo gcb(14); ?></div>
	<div class="single-call-to-action"><?php echo gcb(15); ?></div>
</div>

<div id="event-slider">
	<?php echo do_shortcode('[advps-slideshow optset="3"]'); ?>
</div>

<div id="tracks-header">
	<?php echo gcb(16);?>
</div>

<div id="tracks-wrapper">
	<div id="tracks">
		<div class="single-track"><?php echo gcb(1); ?></div>
		<div class="single-track"><?php echo gcb(2); ?></div>
		<div class="single-track"><?php echo gcb(3); ?></div>
		<div class="single-track"><?php echo gcb(4); ?></div>
		<div class="single-track"><?php echo gcb(5); ?></div>
		<div class="single-track"><?php echo gcb(6); ?></div>
		<div class="single-track"><?php echo gcb(7); ?></div>
		<div class="single-track"><?php echo gcb(8); ?></div>
		<div class="single-track"><?php echo gcb(9); ?></div>
		<div class="single-track"><?php echo gcb(10); ?></div>
		<div class="single-track"><?php echo gcb(11); ?></div>
		<div class="single-track"><?php echo gcb(12); ?></div>
	</div>
</div>

<div id="call-to-action-2" class="call-to-action">
	<div class="single-call-to-action"><?php echo gcb(17); ?></div>
	<div class="single-call-to-action"><?php echo gcb(18); ?></div>
	<div class="single-call-to-action"><?php echo gcb(19); ?></div>
</div>

<div id="twitter-feed">
	<?php echo do_shortcode('[ap-twitter-feed-pro-slider slide_duration="7000"]'); ?>
</div>

<?php
get_footer();
?>


