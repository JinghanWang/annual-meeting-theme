<?php
/**
 * Customized Header for AAM theme.
 *
 * @package Skeleton WordPress Theme
 * @subpackage skeleton
 * @author Simple Themes - www.simplethemes.com
 */
?>
<!doctype html>
<!--[if lt IE 7 ]><html class="ie ie6" <?php language_attributes();?>> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" <?php language_attributes();?>> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" <?php language_attributes();?>> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" <?php language_attributes();?>> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html <?php language_attributes();?>> <!--<![endif]-->

<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->

<title><?php wp_title( '|', true, 'right' ); ?></title>

<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />

<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
<![endif]-->


<!-- Mobile Specific Metas
================================================== -->

<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />


<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

<div id="wrap" class="container">
	<div id="header-wrapper">
		<div id="header" class="sixteen columns">
			<div class="inner">
				<div class="header_extras">
					<?php 
						dynamic_sidebar( 'header-widget' );
					?>
				</div>
				<?php
					$skeleton_logo  = '<h1 id="site-title">';
					$skeleton_logo .= '<a class="logotype-img" href="'.esc_url( home_url( '/' ) ).'" title="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'" rel="home">';
					$skeleton_logo .= '<img src="'.skeleton_options( 'logotype' ).'" alt="'.esc_attr( get_bloginfo( 'name', 'display' ) ).'"></a>';
					$skeleton_logo .= '</h1>';
					$skeleton_logo .= '<div class="site-desc">'.get_bloginfo('description').'</div>'. "\n";
					echo apply_filters ( 'skeleton_child_logo', $skeleton_logo);
				?>
			</div>
		</div>
	</div>
	<div id="nav-wrapper">
		<div id="navigation" class="row sixteen columns">
			<?php wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary')); ?>
			<div class="clear"></div>
		</div>
	</div>